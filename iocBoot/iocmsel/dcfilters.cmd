#!../../bin/linux-x86_64/msel

## You may have to change msel to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/msel.dbd"
msel_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/msel.db","BL=PINK,DEV=DCFLT1,MOTOR=PINK:SMA01:m0")
dbLoadRecords("db/msel.db","BL=PINK,DEV=DCFLT2,MOTOR=PINK:SMA01:m1")
dbLoadRecords("db/msel.db","BL=PINK,DEV=DCFLT3,MOTOR=PINK:SMA01:m2")
dbLoadRecords("db/msel.db","BL=PINK,DEV=DCFLT4,MOTOR=PINK:SMA01:m5")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("${TOP}/iocBoot/${IOC}/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK")

## Start any sequence programs
#seq sncxxx,"user=epics"
