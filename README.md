# pink-dcfilters

EPICS IOC to control PINK diagnostic chamber filters

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-bl/pink-dcfilters/dcfilters:v1.0
    container_name: dcfilters
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pink-dcfilters/iocBoot/iocmsel"
    command: "./dcfilters.cmd"
    volumes:
      - type: bind
        source: /EPICS/autosave/dcfilters
        target: /EPICS/autosave
```
